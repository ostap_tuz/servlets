package com.epam.domain;

public class Product {
    public static int id_unique = 1;
    private int id = id_unique;
    private String productName;
    private int amount;

    public Product(String productName, int amount) {
        this.productName = productName;
        this.amount = amount;
        this.id = id_unique;
        id_unique++;
    }

    public String getProductName() {
        return productName;
    }

    public int getAmount() {
        return amount;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return id+". "+productName+" - "+amount+" things";
    }
}
