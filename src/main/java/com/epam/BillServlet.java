package com.epam;

import com.epam.domain.Product;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;


@WebServlet("/bills/*")
public class BillServlet extends HttpServlet {

    private static Logger logger = LogManager.getLogger(BillServlet.class );
    private static Map<Integer, Product> products = new HashMap<>();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Products\n-------------\n");
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.println("<html><body>");
        writer.println("<a href='bills'>Refresh</a>");
        writer.println("<h1 style=\"background-color:#970000; color:#ffffff\">Products:</h1>");

        for (Product product: products.values()) {
            writer.println("<p>"+product+"</p>");
        }

        writer.println("<form action='bills' method='POST'>\n" +
                "<br>Input product:<input type=text name='product-name'>\n" +
                "<br>Input amount: <input type=text name='product-amount'>\n" +
                "<br><button type='submit'>Add product</button>\n" +
                "</form>");

        writer.println("<h1 style=\"background-color:#970000; color:#ffffff\">DELETE PRODUCT</h1>");
        writer.println("<form>" +
                "Id: <input type='text' name=product-id>" +
                "<br><input type='button' onclick='remove(this.form.product-id.value)' name='ok' value='Delete'" +
                "</form>");

        writer.println("<script type='text/javascript'>\n"
                + "  function remove(id) { fetch('bills/' + id, {method: 'DELETE'}); }\n"
                + "</script>");



        writer.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String readProductName = req.getParameter("product-name");
        String readProductValue = req.getParameter("product-amount");
        Product newProduct = new Product(readProductName, Integer.parseInt(readProductValue));
        products.put(newProduct.getId(),newProduct);
        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newId = req.getRequestURI();
        newId = newId.replaceAll("/ProductsREST-1.0-SNAPSHOT/bills/", "");
        products.remove(Integer.parseInt(newId));
    }
}
